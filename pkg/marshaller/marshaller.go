package marshaller

import (
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/nikolatw/be-code-challenge-master/gen/go/proto/fees"
	"google.golang.org/protobuf/encoding/protojson"
)

// Marshaller is patched grpc-gateway.runtime.JSONPb marshaller.
type Marshaller struct {
	runtime.JSONPb
}

// Marshal marshals message into JSON.
func (m *Marshaller) Marshal(message interface{}) ([]byte, error) {
	return m.JSONPb.Marshal(m.dispatch(message))
}

func (Marshaller) dispatch(message interface{}) interface{} {
	switch v := message.(type) {
	case *fees.Fees:
		return v.Fees
	default:
		return v
	}
}

// MuxOption returns patched grpc-gateway.runtime.JSONPb marshaller.
func MuxOption() runtime.ServeMuxOption {
	return runtime.WithMarshalerOption(runtime.MIMEWildcard, &Marshaller{
		runtime.JSONPb{
			MarshalOptions: protojson.MarshalOptions{
				EmitUnpopulated: true,
			},
		},
	})
}
