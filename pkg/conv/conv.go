package conv

const (
	// WeiInETH amount of Wei in one ETH.
	WeiInETH float64 = 10 ^ 18
)

// ConvertWeiToETH returns amount of ETH in wei.
func ConvertWeiToETH(wei float64) float64 {
	return wei / WeiInETH
}
