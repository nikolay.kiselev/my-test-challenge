SELECT SUM(t.gas_used * t.gas_price) "used",
    DATE_TRUNC('hour', t.block_time) "at"
FROM transactions t
    LEFT JOIN contracts c ON c.address = t.to
    OR c.address = t.from
WHERE c.address IS NULL
    AND "to" <> '0x0000000000000000000000000000000000000000'
GROUP BY DATE_TRUNC('hour', t.block_time);