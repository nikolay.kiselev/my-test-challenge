package database

import (
	"context"
	"database/sql"
	_ "embed"

	"github.com/rs/zerolog/log"
)

// Storage is structure with methods to work with SQL databases.
type Storage struct {
	Adaptor *sql.DB
}

//go:embed sql/fees.sql
var queryFees string

// CreateIndex safe create index in database.
func (storage Storage) CreateIndex(name, index string) {
	_, err := storage.Adaptor.Exec(index)
	if err != nil {
		log.Warn().Err(err).Str("index", name).Msg("failed to create index")
	}
}

// Fees return gas price * gas used, and sum it by hour.
func (storage Storage) Fees(ctx context.Context) (chan Fee, error) {
	stream := make(chan Fee)

	rows, err := storage.Adaptor.QueryContext(ctx, queryFees)

	go func() {
		defer rows.Close()

		for rows.Next() {
			var fee Fee
			err = rows.Scan(&fee.UsedWei, &fee.At)
			if err != nil {
				log.Info().Err(err).Send()
				break
			}
			stream <- fee
		}

		close(stream)
	}()

	return stream, nil
}
