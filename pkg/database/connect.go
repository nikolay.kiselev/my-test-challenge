package database

import (
	"database/sql"
)

// ConnectPostgres returns storage, requires posgres dsn.
func ConnectPostgres(dsn string) (*Storage, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	return &Storage{Adaptor: db}, nil
}
