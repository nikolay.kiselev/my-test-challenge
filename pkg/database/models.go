package database

import (
	"time"

	"github.com/nikolatw/be-code-challenge-master/pkg/conv"
)

// Fee is model of fee related to sql/fees.sql.
type Fee struct {
	UsedWei float64
	At      time.Time
}

// UsedETH returns how much gas was used in ETH.
func (model Fee) UsedETH() float64 {
	return conv.ConvertWeiToETH(model.UsedWei)
}

// Timestamp returns Unix timestamp for timezone.
func (model Fee) Timestamp(location *time.Location) int64 {
	if location == nil {
		return model.At.Unix()
	}

	_, offset := model.At.In(location).Zone()

	return model.At.Add(time.Duration(offset) * time.Second).Unix()
}
