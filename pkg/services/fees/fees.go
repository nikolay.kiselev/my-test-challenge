package fees

import (
	"context"
	"time"

	pb "github.com/nikolatw/be-code-challenge-master/gen/go/proto/fees"
	"github.com/nikolatw/be-code-challenge-master/pkg/database"
	"github.com/patrickmn/go-cache"
)

// Fees service methods to read fees in the Ethereum network.
type Fees struct {
	pb.UnimplementedFeesServiceServer

	storage *database.Storage
	cache   *cache.Cache
}

const (
	cacheExpire = 5 * time.Minute
	cacheClean  = 10 * time.Minute
)

// New returns initialised `Fees` service.
func New(storage *database.Storage) *Fees {
	return &Fees{
		storage: storage,
		cache:   cache.New(cacheExpire, cacheClean),
	}
}

// GetFees return stream of fees in the Ethereum network have been spent by plain ETH transfers.
func (service Fees) GetFees(ctx context.Context, request *pb.FeesRequest) (*pb.Fees, error) {
	key := request.GetTimezone()
	cached, ok := service.cache.Get(key)
	if ok {
		return cached.(*pb.Fees), nil
	}

	response, err := service.fees(ctx, request)
	if err != nil {
		return nil, err
	}

	go service.cache.SetDefault(key, response)

	return response, nil
}

func (service *Fees) fees(ctx context.Context, request *pb.FeesRequest) (*pb.Fees, error) {
	location, err := getRequestLocation(request)
	if err != nil {
		return nil, err
	}

	response := pb.Fees{}

	feeStream, err := service.storage.Fees(ctx)
	if err != nil {
		return nil, err
	}
	for fee := range feeStream {
		response.Fees = append(response.Fees, &pb.Fee{
			T: int32(fee.Timestamp(location)),
			V: fee.UsedWei,
		})
	}

	return &response, nil
}

func getRequestLocation(request *pb.FeesRequest) (*time.Location, error) {
	var location *time.Location

	if request.Timezone != nil {
		requestedLocation, err := time.LoadLocation(request.GetTimezone())
		if err != nil {
			return nil, err
		}
		location = requestedLocation
	}

	return location, nil
}
