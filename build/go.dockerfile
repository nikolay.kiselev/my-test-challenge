FROM golang AS builder

ARG APP_PATH=cmd/proxy

WORKDIR /tmp/build/

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY gen/ ./gen/
COPY pkg/ ./pkg/
COPY ${APP_PATH}/ ${APP_PATH}/

ENV CGO_ENABLED 0
RUN go build -o /opt/service/app ./${APP_PATH}/
RUN chmod +x /opt/service/app


FROM alpine

RUN apk add --no-cache tzdata

WORKDIR /opt/service/

COPY --from=builder /opt/service/ /opt/service/

CMD ["/opt/service/app"]
