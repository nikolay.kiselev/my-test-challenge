// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        (unknown)
// source: proto/fees/fees.proto

package fees

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type FeesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Timezone *string `protobuf:"bytes,1,opt,name=timezone,proto3,oneof" json:"timezone,omitempty"`
}

func (x *FeesRequest) Reset() {
	*x = FeesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_fees_fees_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *FeesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*FeesRequest) ProtoMessage() {}

func (x *FeesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_fees_fees_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use FeesRequest.ProtoReflect.Descriptor instead.
func (*FeesRequest) Descriptor() ([]byte, []int) {
	return file_proto_fees_fees_proto_rawDescGZIP(), []int{0}
}

func (x *FeesRequest) GetTimezone() string {
	if x != nil && x.Timezone != nil {
		return *x.Timezone
	}
	return ""
}

type Fee struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	T int32   `protobuf:"varint,1,opt,name=t,proto3" json:"t,omitempty"`
	V float64 `protobuf:"fixed64,2,opt,name=v,proto3" json:"v,omitempty"`
}

func (x *Fee) Reset() {
	*x = Fee{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_fees_fees_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Fee) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Fee) ProtoMessage() {}

func (x *Fee) ProtoReflect() protoreflect.Message {
	mi := &file_proto_fees_fees_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Fee.ProtoReflect.Descriptor instead.
func (*Fee) Descriptor() ([]byte, []int) {
	return file_proto_fees_fees_proto_rawDescGZIP(), []int{1}
}

func (x *Fee) GetT() int32 {
	if x != nil {
		return x.T
	}
	return 0
}

func (x *Fee) GetV() float64 {
	if x != nil {
		return x.V
	}
	return 0
}

type Fees struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Fees []*Fee `protobuf:"bytes,1,rep,name=fees,proto3" json:"fees,omitempty"`
}

func (x *Fees) Reset() {
	*x = Fees{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_fees_fees_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Fees) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Fees) ProtoMessage() {}

func (x *Fees) ProtoReflect() protoreflect.Message {
	mi := &file_proto_fees_fees_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Fees.ProtoReflect.Descriptor instead.
func (*Fees) Descriptor() ([]byte, []int) {
	return file_proto_fees_fees_proto_rawDescGZIP(), []int{2}
}

func (x *Fees) GetFees() []*Fee {
	if x != nil {
		return x.Fees
	}
	return nil
}

var File_proto_fees_fees_proto protoreflect.FileDescriptor

var file_proto_fees_fees_proto_rawDesc = []byte{
	0x0a, 0x15, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x66, 0x65, 0x65, 0x73, 0x2f, 0x66, 0x65, 0x65,
	0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x66, 0x65, 0x65, 0x73, 0x1a, 0x1c, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x3b, 0x0a, 0x0b, 0x46,
	0x65, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x1f, 0x0a, 0x08, 0x74, 0x69,
	0x6d, 0x65, 0x7a, 0x6f, 0x6e, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x48, 0x00, 0x52, 0x08,
	0x74, 0x69, 0x6d, 0x65, 0x7a, 0x6f, 0x6e, 0x65, 0x88, 0x01, 0x01, 0x42, 0x0b, 0x0a, 0x09, 0x5f,
	0x74, 0x69, 0x6d, 0x65, 0x7a, 0x6f, 0x6e, 0x65, 0x22, 0x21, 0x0a, 0x03, 0x46, 0x65, 0x65, 0x12,
	0x0c, 0x0a, 0x01, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x01, 0x74, 0x12, 0x0c, 0x0a,
	0x01, 0x76, 0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x01, 0x76, 0x22, 0x25, 0x0a, 0x04, 0x46,
	0x65, 0x65, 0x73, 0x12, 0x1d, 0x0a, 0x04, 0x66, 0x65, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28,
	0x0b, 0x32, 0x09, 0x2e, 0x66, 0x65, 0x65, 0x73, 0x2e, 0x46, 0x65, 0x65, 0x52, 0x04, 0x66, 0x65,
	0x65, 0x73, 0x32, 0x42, 0x0a, 0x0b, 0x46, 0x65, 0x65, 0x73, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x12, 0x33, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x46, 0x65, 0x65, 0x73, 0x12, 0x11, 0x2e, 0x66,
	0x65, 0x65, 0x73, 0x2e, 0x46, 0x65, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x0a, 0x2e, 0x66, 0x65, 0x65, 0x73, 0x2e, 0x46, 0x65, 0x65, 0x73, 0x22, 0x09, 0x82, 0xd3, 0xe4,
	0x93, 0x02, 0x03, 0x12, 0x01, 0x2f, 0x42, 0x1e, 0x5a, 0x1c, 0x67, 0x6c, 0x61, 0x73, 0x73, 0x6e,
	0x6f, 0x64, 0x65, 0x73, 0x6f, 0x6c, 0x75, 0x74, 0x69, 0x6f, 0x6e, 0x2f, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2f, 0x66, 0x65, 0x65, 0x73, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_proto_fees_fees_proto_rawDescOnce sync.Once
	file_proto_fees_fees_proto_rawDescData = file_proto_fees_fees_proto_rawDesc
)

func file_proto_fees_fees_proto_rawDescGZIP() []byte {
	file_proto_fees_fees_proto_rawDescOnce.Do(func() {
		file_proto_fees_fees_proto_rawDescData = protoimpl.X.CompressGZIP(file_proto_fees_fees_proto_rawDescData)
	})
	return file_proto_fees_fees_proto_rawDescData
}

var file_proto_fees_fees_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_proto_fees_fees_proto_goTypes = []interface{}{
	(*FeesRequest)(nil), // 0: fees.FeesRequest
	(*Fee)(nil),         // 1: fees.Fee
	(*Fees)(nil),        // 2: fees.Fees
}
var file_proto_fees_fees_proto_depIdxs = []int32{
	1, // 0: fees.Fees.fees:type_name -> fees.Fee
	0, // 1: fees.FeesService.GetFees:input_type -> fees.FeesRequest
	2, // 2: fees.FeesService.GetFees:output_type -> fees.Fees
	2, // [2:3] is the sub-list for method output_type
	1, // [1:2] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_proto_fees_fees_proto_init() }
func file_proto_fees_fees_proto_init() {
	if File_proto_fees_fees_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_proto_fees_fees_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*FeesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_fees_fees_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Fee); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_fees_fees_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Fees); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_proto_fees_fees_proto_msgTypes[0].OneofWrappers = []interface{}{}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_proto_fees_fees_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_proto_fees_fees_proto_goTypes,
		DependencyIndexes: file_proto_fees_fees_proto_depIdxs,
		MessageInfos:      file_proto_fees_fees_proto_msgTypes,
	}.Build()
	File_proto_fees_fees_proto = out.File
	file_proto_fees_fees_proto_rawDesc = nil
	file_proto_fees_fees_proto_goTypes = nil
	file_proto_fees_fees_proto_depIdxs = nil
}
