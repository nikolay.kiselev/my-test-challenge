package main

import (
	"flag"
	"fmt"
	"net"
	"os"

	_ "github.com/lib/pq"
	pb "github.com/nikolatw/be-code-challenge-master/gen/go/proto/fees"
	"github.com/nikolatw/be-code-challenge-master/pkg/database"
	"github.com/nikolatw/be-code-challenge-master/pkg/services/fees"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
)

var port = flag.Int("port", 9591, "The server port")

func main() {
	flag.Parse()

	lis, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", *port))
	if err != nil {
		log.Fatal().Err(err).Msg("failed to listen")
	}

	grpcServer := getGRPCServer()

	log.Info().Msg("Service is starting")

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
}

func getGRPCServer() *grpc.Server {
	storage, err := database.ConnectPostgres(os.Getenv("PG_DSN"))
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect postgres")
	}

	grpcServer := grpc.NewServer()
	pb.RegisterFeesServiceServer(grpcServer, fees.New(storage))

	return grpcServer
}
