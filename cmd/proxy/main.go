package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/nikolatw/be-code-challenge-master/gen/go/proto/fees"
	"github.com/nikolatw/be-code-challenge-master/pkg/marshaller"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	port = flag.Int("port", 80, "The server port")

	grpcFeesService = flag.String("fees", "localhost:9591", "fees gRPC service server endpoint")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux(marshaller.MuxOption())
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}

	err := fees.RegisterFeesServiceHandlerFromEndpoint(ctx, mux, *grpcFeesService, opts)
	if err != nil {
		return err
	}

	log.Info().Msg("Service is starting")

	return http.ListenAndServe(fmt.Sprintf(":%d", *port), mux)
}

func main() {
	flag.Parse()

	if err := run(); err != nil {
		log.Fatal().Err(err).Send()
	}
}
