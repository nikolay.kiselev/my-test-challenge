# Goal

Analyze how much fees were spent by users on different
kinds of actions

# Tasks

- [x] create service that will return how much fees in the Ethereum network have been spent by plain **ETH** transfers
    - [x] API to serve that information to the public
    - [x] service available on `8080` port
    - [x] endpoint which serves data in the right JSON format
        - [x] `t` is a unix timestamp of the hour
        - [x] `v` is the amount of fees being paid for transactions
- [x] compute the hourly amount of fees spent by transactions
    - [x] transaction are between two **EOA** addresses
- [x] fee computation is done in the following fashion: `gas_used * gas_price`
    - [x] Gas price is converted from Wei to ETH

# Solution

I decided to use GRPC, because it's quick, relible and maintainable. As database adaptor I selected standart `database/sql`, because there no reason to add complexity according to task scope. To convert GRPC to HTTP API I used [GRPC gateway][], because it's far easier to use, than write HTTP facade by yourself. I decided to cover `Fees` response with cache, to increase speed.

We have trade-off: GRPC doesn't support responses with array, only message marshalled as JSON. Because of that I was forced to patch marshaller. With that detail we have redurant step before marshalling response, and we are unable to generate OpenAPI specification of application. I hope that we can agree on different format of response, so issue will be resolved.

# Usage

To start application
1. install `docker-compose`, [docker-compose instalation][]
2. execute in shell `docker-compose up`
3. go to http://localhost:8080/ to see fees.

By the way, I also added timezone support. You can check it via http://localhost:8080/?timezone=Asia/Shanghai. It's completly optional, and this feature doesn't decrease performance.

# Maintenance

Application writen in [go][], uses [GRPC][] (and [protobuf][]). For development commands application uses [go-task][]. For execution application uses [docker][].

1. install `go`, [go instalation][]
2. install `task`, [go-task instalation][]
3. install `docker`, [docker instalation][]
4. install `docker-compose`, [docker-compose instalation][]
5. setup development enviroment `task setup`

Now you able to run:
+ `task gen` - generate go code from protobuf files
+ `task fmt` - format go code
+ `task lint` - lint go code
+ `task docs` - lint missing docs
+ `task test` - run go tests
+ `task` - composition of gen, fmt, lint, docs, test commands
+ `task compose` - start application in docker compose, detached
+ `task clean` - stop and remove application in docker compose

[GRPC gateway]: https://grpc-ecosystem.github.io/grpc-gateway/
[go]: https://golang.org/
[GRPC]: https://grpc.io/docs/languages/go/
[protobuf]: https://developers.google.com/protocol-buffers
[go-task]: https://taskfile.dev/
[docker]: https://docs.docker.com/get-started/overview/
[go instalation]: https://go.dev/doc/install
[go-task instalation]: https://taskfile.dev/installation/
[docker instalation]: https://docs.docker.com/engine/install/
[docker-compose instalation]: https://docs.docker.com/compose/install/
